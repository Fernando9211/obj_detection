#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import subprocess
import cv2
import sys
import os
import codecs
import glob
from collections import defaultdict
import logging
logger = logging.getLogger(__name__)

try:
    import wrappers.pytesseract2 as pytesseract2
except ImportError as e:
    import pytesseract2 as pytesseract2

pdfinfo_command = "/usr/bin/pdfinfo"
pdfimages_command = "/usr/bin/pdfimages"
pdftotext_command = "/usr/bin/pdftotext"
ghostscript_command = "/usr/bin/gs"
DEFAULT_LANG = "ita"


class PyPDFConverterWrapper():

    def __init__(self, filename, output_filename_root=None):
        print(filename)
        self.filename = filename
        self.info = {}
        if output_filename_root is None:
            self.output_filename_root = "{}-".format(self.filename[:-4])
        else:
            self.output_filename_root = output_filename_root
        self.output_filename_image = "{}image".format(self.output_filename_root)
        self.output_filename_text = "{}text.txt".format(self.output_filename_root)
        self.output_dict = {}

    def pdfinfo(self):
        """
        Wraps command line utility pdfinfo to extract the PDF meta information.
        Returns metainfo in a dictionary.
        sudo apt-get install poppler-utils
        This function parses the text output that looks like this:
            Title:          PUBLIC MEETING AGENDA
            Author:         Customer Support
            Creator:        Microsoft Word 2010
            Producer:       Microsoft Word 2010
            CreationDate:   Thu Dec 20 14:44:56 2012
            ModDate:        Thu Dec 20 14:44:56 2012
            Tagged:         yes
            Pages:          2
            Encrypted:      no
            Page size:      612 x 792 pts (letter)
            File size:      104739 bytes
            Optimized:      no
            PDF version:    1.5
        """

        infile = self.filename
        cmd = pdfinfo_command

        def _extract(row):
            """Extracts the right hand value from a : delimited row"""
            return row.split(':', 1)[1].strip()

        # output = {}

        labels = ['Title', 'Author', 'Creator', 'Producer', 'CreationDate',
                  'ModDate', 'Tagged', 'Pages', 'Encrypted', 'Page size',
                  'File size', 'Optimized', 'PDF version']

        command = "{} {}".format(cmd, infile)
        cmd_output = subprocess.check_output(command, universal_newlines=True, shell=True)
        for line in cmd_output.splitlines():
            for label in labels:
                if label in line:
                    # output[label] = _extract(line)
                    self.info[label] = _extract(line)

        # return output

    def pdfimages(self):
        command = "{} -list {}".format(pdfimages_command, self.filename)
        cmd_output = subprocess.check_output(command, universal_newlines=True, shell=True)
        list_lines = cmd_output.splitlines()
        if len(list_lines) <= 2:
            return 0
        else:
            return len(list_lines)-2

        '''
        labels = ['page', 'num', 'type', 'width', 'height', 'color', 'comp', 'bpc', 'enc', 'interp', 'object', 'ID',
                  'x-ppi', 'y-ppi', 'size', 'ratio']

        output_images = []
        if len(list_lines) <= 2:
            print("No images found")
        else:
            """
            for i in range(2, len(list_lines)):
                line = list_lines[i]
                # print("Line: {}".format(line))
                # print("Line split: {}".format(line.split()))
                current_images = {}
                for j, label in enumerate(labels):
                    current_images[label] = line.split()[j]
                output_images.append(current_images)

            for i, elem in enumerate(output_images):
                # print("IMG {}".format(i))
                # print(elem)
            """
        return output_images
        '''

    def pdfimages_extract(self):
        command = [pdfimages_command, '-all', self.filename, self.output_filename_image]
        proc = subprocess.Popen(command, stderr=subprocess.PIPE, universal_newlines=True)
        status = proc.wait()
        error_string = proc.stderr.read()
        proc.stderr.close()
        return status, error_string

    def pdftotext_extract(self):
        command = [pdftotext_command, self.filename, self.output_filename_text]
        proc = subprocess.Popen(command, stderr=subprocess.PIPE, universal_newlines=True)
        status = proc.wait()
        error_string = proc.stderr.read()
        proc.stderr.close()
        return status, error_string

    def ghostscript_convert(self):
        command = [ghostscript_command, '-dNOPAUSE', '-dSAFER']
        command += ['-sDEVICE=png16m']
        command += ['-r300']
        command += ["-sOutputFile=" + self.output_filename_image + "-%d.png"]
        command += ["-f", self.filename]
        command += ["-c", "quit"]
        proc = subprocess.Popen(command, stderr=subprocess.PIPE, universal_newlines=True)
        status = proc.wait()
        error_string = proc.stderr.read()
        proc.stderr.close()
        return status, error_string

    def use_tesseract(self):
        logger.info("Using tesseract and save it to {}".format(self.output_filename_text))
        with codecs.open(self.output_filename_text, "w", "utf-8") as outputfile:
            for current_image in glob.glob('{}*'.format(self.output_filename_image)):
                logger.debug("Tesseract on image {}".format(current_image))
                image = cv2.imread(current_image)
                current_output_string = pytesseract2.image_to_string(image, lang=DEFAULT_LANG)
                # logger.debug(current_output_string)
                outputfile.write(current_output_string)
                outputfile.write("\n\n")

    def extract_data(self, totext=False):
        logger.info("EXTRACT DATA")
        self.pdfinfo()
        num_pages = int(self.info['Pages'])
        num_images = int(self.pdfimages())
        logger.debug("Num images into pdf: {}".format(num_images))
        logger.debug("Num pages into pdf: {}".format(num_pages))

        logger.debug("FILENAME ROOT: {}".format(self.output_filename_root))

        logger.info("Using Ghostscript converting")
        self.ghostscript_convert()
        if totext is True:
            logger.info("Using pdftotext")
            self.pdftotext_extract()


        """
        if num_pages == num_images: # Salva immagini estraendole dal pdf
            self.output_dict['type'] = "TrueImages"
            logger.info("Using pdfimages converting")
            self.pdfimages_extract()
            if totext is True:
                logger.info("Using tesseract 4.0")
                self.use_tesseract()
        elif num_pages < num_images:
            self.output_dict['type'] = "Mix"
            logger.info("Using Ghostscript converting")
            self.ghostscript_convert()
            if totext is True:
                logger.info("Using tesseract 4.0")
                self.use_tesseract()
        else:
            self.output_dict['type'] = "TrueText"
            logger.info("Using Ghostscript converting")
            self.ghostscript_convert()
            if totext is True:
                logger.info("Using pdftotext")
                self.pdftotext_extract()
        """
        self.collect_output_filename()


    def collect_output_filename(self):
        self.output_dict['all'] = []
        for name in glob.glob('{}*'.format(self.output_filename_root)):
            self.output_dict['all'].append(name)

        self.output_dict['images'] = []
        for name in glob.glob('{}*'.format(self.output_filename_image)):
            self.output_dict['images'].append(name)

        self.output_dict['text'] = []
        for name in glob.glob('{}*'.format(self.output_filename_text)):
            self.output_dict['text'].append(name)

    def get_list_images(self):
        return self.output_dict

    def get_info(self):
        logger.info("PDF Filename: {}".format(self.filename))
        self.pdfinfo()
        logger.debug(self.info)
        num_page = self.info['Pages']
        logger.debug("Num pages: {}".format(num_page))

        output_images = self.pdfimages()
        num_images = len(output_images)
        logger.debug("Num images: {}".format(num_images))


if __name__ == '__main__':
    folder = sys.argv[1]
    files=glob.glob(folder+"/*.PDF")
    for fl in files:
        pdf = PyPDFConverterWrapper(fl)
        # Save image
        # pdf.extract_data()
        # Save also Text
        pdf.extract_data(totext=True)
        print("parte la conversione")
        lista_file = pdf.get_list_images()
        print(lista_file)


        # pdf.get_info()